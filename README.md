# SubLord

Web App for text file manipulation (subtitles mainly)

### Prerequisites
node.js

### Live demo
[tomas223.gitlab.io/sublord-react](https://tomas223.gitlab.io/sublord-react/)

or

[sublord.evocative.space](http://http://sublord.evocative.space/)

### Run locally

```
git clone git@gitlab.com:tomas223/sublord-react.git
cd sublord-react
npm i
npm start
```

Go to:
[localhost:3000](http://localhost:3000/)