import React, {Component} from 'react';
import Swipeable from 'react-swipeable';

import './scss/App.scss';
import './components/DropFileComponent';

import DropFileComponent from "./components/DropFileComponent";
import DownloadFileComponent from "./components/DownloadFileComponent";
import SideDockSettingsComponent from "./components/SideDockSettingsComponent";
import HeaderComponent from "./components/Header";

class App extends Component {
    constructor() {
        super();
        this.state = {
            files: [],
            settings: {},
            isDockVisible: false,
        }
    }

    updateFiles(files) {
        this.setState({files});
    }

    updateSettings = (settings) => {
        console.log("Updating settings!");
        this.setState({settings});
    };

    toggleDock = (openDock) => {
        this.setState((prevState) => {
            const {isDockVisible} = prevState;
            switch (openDock) {
                case true:
                    if (!isDockVisible) return {isDockVisible: true};
                    break;
                case false:
                    if (isDockVisible) return {isDockVisible: false};
                    break;
                default:
                    return {isDockVisible: !isDockVisible};
            }
        })
    };

    render() {
        const {files, settings, isDockVisible} = this.state;
        return (
            <Swipeable
                onSwipingLeft={() => this.toggleDock(false)}
                onSwipingRight={() => this.toggleDock(true)}
            >
                <SideDockSettingsComponent
                    isVisible={isDockVisible}
                    toggleDock={this.toggleDock}
                    settings={settings}
                    onNewSettings={this.updateSettings}
                />
                <div className="App">

                    <HeaderComponent
                        toggleDock={this.toggleDock}
                        language={settings.language}
                    />
                    <div className="content">
                        <main className="main container">
                            <DropFileComponent
                                files={files}
                                onNewFiles={f => this.updateFiles(f)}
                            />
                            <DownloadFileComponent
                                files={files}
                                languageSettings={settings.language}
                            />
                        </main>
                    </div>
                    <footer className="footer container">
                        <p>
                            © {new Date().getFullYear()} by tomas223
                        </p>
                    </footer>
                </div>
            </Swipeable>
        );
    }
}

export default App;
