
const DEFAULT_ENCODING = "UTF-8";

function readFileAsText(inputFile, encoding) {
    const reader = new FileReader();

    return new Promise((resolve, reject) => {
        reader.onerror = () => {
            reader.abort();
            reject(new DOMException("Problem parsing input file."));
        };

        reader.onload = () => {
            resolve(reader.result);
        };

        reader.readAsText(inputFile, encoding);
    });
}

function replaceChars(string, charMap) {
    console.log(charMap);
    let newString = string;
    for (let chars of charMap) {
        if (!chars.disabled) {
            newString = newString.replace(new RegExp(chars.from, "g"), chars.to);
            if (chars.from !== chars.from.toUpperCase()) {
                newString = newString.replace(new RegExp(chars.from.toUpperCase(), "g"), chars.to.toUpperCase());
            }
        }
    }
    return newString;
}


export async function convertFile(file, charMap, encoding) {
    const fileEncoding = encoding || DEFAULT_ENCODING;

    const text = await readFileAsText(file, fileEncoding);
    const newText = replaceChars(text, charMap);

    const fileType = file.type || "text/plain";
    const blob =  new Blob([newText], {
        type: `${fileType};${fileEncoding}`
    });

    return new File([blob], file.name);
}