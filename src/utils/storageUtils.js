
const sessionStorage = window.localStorage;
const LANG_STRG = "LANGUAGES_SETTINGS";


const isDataStored = (dataName) => (
    !!sessionStorage.getItem(dataName)
);

const getData = (dataName) => (
    isDataStored(dataName) ? JSON.parse(sessionStorage.getItem(dataName)) : null
);

const storeObject = (data = undefined, storageName) => {
    if (data && storageName) {
        if (typeof(data) !== "object") {
            return console.error("storeObject() expects Object as first argument");
        } else if (typeof(storageName) !== "string") {
            return console.error("storeObject() expects string as second argument");
        } else {
            // data.timestamp = new Date().toISOString();
            sessionStorage.setItem(storageName, JSON.stringify(data));
        }
    }
};

export const getLanguagesSettings = () => (
    getData(LANG_STRG)
);

export const storeLanguage = (language) => {
    const newLanguagesSettings = {...getLanguagesSettings()};
    newLanguagesSettings[language.id] = {
        ...language,
        timestamp: Date.now()
    };
    storeObject({
        ...newLanguagesSettings,
    }, LANG_STRG);
};

export const removeLanguage = (langId) => {
    const newLanguagesSettings = {...getLanguagesSettings()};
    delete newLanguagesSettings[langId];
    storeObject({
        ...newLanguagesSettings,
    }, LANG_STRG);
};

export const removeSession = () => {
    console.log("INFO: removing session");
    sessionStorage.removeItem(LANG_STRG);
};
