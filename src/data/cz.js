
const charMap = [
    {from: "á", to: "a", disabled: true},
    {from: "č", to: "c"},
    {from: "ď", to: "d"},
    {from: "é", to: "e"},
    {from: "ě", to: "e"},
    {from: "í", to: "i"},
    {from: "ň", to: "n"},
    {from: "ó", to: "o"},
    {from: "ř", to: "r"},
    {from: "š", to: "s"},
    {from: "ť", to: "t"},
    {from: "ú", to: "u"},
    {from: "ů", to: "u"},
    {from: "ý", to: "y"},
    {from: "ž", to: "z"},
];

export default {
    id: "cz",
    longLabel: "Čeština",
    shortLabel: "CZ",
    encoding: "windows-1250",
    charMap
};