import sk from "./sk";
import pl from "./pl";
import cz from "./cz";

const languagesData = {};
languagesData[sk.id] = sk;
languagesData[cz.id] = cz;
languagesData[pl.id] = pl;

export default languagesData;