
const charMap = [
    {from:"á", to: "a", disabled: true},
    {from:"ä", to: "a"},
    {from:"č", to: "c"},
    {from:"ď", to: "d"},
    {from:"é", to: "e"},
    {from:"í", to: "i"},
    {from:"ľ", to: "l"},
    {from:"ň", to: "n"},
    {from:"ô", to: "o"},
    {from:"ó", to: "o"},
    {from:"š", to: "s"},
    {from:"ť", to: "t"},
    {from:"ú", to: "u"},
    {from:"ý", to: "y"},
    {from:"ž", to: "z"},
];

export default {
    id: "sk",
    longLabel: "Slovenčina",
    shortLabel: "SK",
    encoding: "windows-1250",
    charMap
};