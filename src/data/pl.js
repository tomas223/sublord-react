
const charMap = [
    {from: "ą", to: "a"},
    {from: "ć", to: "c"},
    {from: "ę", to: "e"},
    {from: "ł", to: "l"},
    {from: "ń", to: "n"},
    {from: "ó", to: "o"},
    {from: "ś", to: "s"},
    {from: "ź", to: "z"},
    {from: "ż", to: "z"},
];

export default {
    id: "pl",
    longLabel: "Polski",
    shortLabel: "PL",
    encoding: "windows-1250",
    charMap
};