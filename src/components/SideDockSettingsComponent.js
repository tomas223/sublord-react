import React, {Component} from "react";
import Dock from "react-dock";
import Select from 'react-select';
import 'react-select/dist/react-select.css';

import languagesData from "../data/languages";
import {getLanguagesSettings, storeLanguage, removeLanguage} from "../utils/storageUtils";
import Button from "./Button";

const MOBILE_VIEWPORT_SIZE = 630;


export default class SideDockSettingsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            languageSelectOptions: [],
            selectedLanguage: null,

            languageSettings: {},
            notSavedChanges: false,

            isMobileViewport: true,
        }
    }

    windowsResizedHandler = (event) => {
        this.windowResized(event.target.innerWidth);
    };

    windowResized(windowWidth) {
        const {isMobileViewport} = this.state;
        if (isMobileViewport !== (parseInt(windowWidth, 10) < MOBILE_VIEWPORT_SIZE)) {
            this.setState({isMobileViewport: !isMobileViewport})
        }
    }

    componentWillMount() {
        window.addEventListener('resize', this.windowsResizedHandler);
        this.windowResized(window.innerWidth);
        this.initData();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.windowsResizedHandler);
    }

    initData = (newLangId) => {
        const languageSelectOptions = [];

        const storageSettings = getLanguagesSettings();
        const defaultSettings = languagesData;

        const allLanguagesSettings = {
            ...defaultSettings,
            ...storageSettings,
        };

        let selectedLanguage = null;
        let languageSettings = null;

        for (const lang in allLanguagesSettings) {
            const {id, longLabel} = allLanguagesSettings[lang];
            languageSelectOptions.push({value: id, label: longLabel});
            if (newLangId && newLangId === id) {
                selectedLanguage = {value: id, label: longLabel};
                languageSettings = allLanguagesSettings[lang];
            }
        }

        if (newLangId === undefined) {
            const {value, label} = languageSelectOptions[0];
            selectedLanguage = {value, label};
            languageSettings = allLanguagesSettings[value];
        }

        this.setState({
            languageSelectOptions,
            languageSettings,
            selectedLanguage,
            notSavedChanges: false
        });

        this.updateSettings({
            language: languageSettings
        });
    };

    changeLanguage = (newLang) => {
        console.log(newLang);
        this.initData(newLang && newLang.value);
    };

    updateSettings = (newSetting) => {
        const {settings, onNewSettings} = this.props;
        onNewSettings({
            ...settings,
            ...newSetting,
        })
    };

    toggleChar = (charNum) => {
        // const {checked} = event.target;
        const targetIndex = parseInt(charNum, 10);

        this.setState((prevState) => {
            const {languageSettings} = prevState;
            const newCharMap = languageSettings.charMap.map((value, index) =>
                index === targetIndex
                    ? {...value, disabled: !value.disabled}
                    : value
            );

            return {
                notSavedChanges: true,
                languageSettings: {
                    ...languageSettings,
                    charMap: newCharMap,
                },
            }
        });
    };

    removeChar = (charNum) => {
        const targetIndex = parseInt(charNum, 10);

        this.setState((prevState) => {
            const {charMap} = prevState.languageSettings;
            const newCharMap = [
                ...charMap.slice(0, targetIndex),
                ...charMap.slice(targetIndex + 1),
            ];

            return {
                notSavedChanges: true,
                languageSettings: {
                    ...prevState.languageSettings,
                    charMap: newCharMap,
                },
            }
        });
    };


    saveSettings = () => {
        const {languageSettings} = this.state;
        storeLanguage(languageSettings);
        this.initData(languageSettings.id);
    };

    resetSettings = () => {
        const {languageSettings} = this.state;
        removeLanguage(languageSettings.id);
        this.initData(languageSettings.id);
    };

    render() {
        const {languageSelectOptions, selectedLanguage, languageSettings,
            notSavedChanges, isMobileViewport} = this.state;
        const {isVisible, toggleDock} = this.props;
        return (
            <div className="side-dock">
                <Dock
                    position='left'
                    isVisible={isVisible}
                    className="side-dock"
                    duration={200}
                    // dimMode="opaque"
                    zIndex={100}
                    defaultSize={isMobileViewport ? 0.8 : MOBILE_VIEWPORT_SIZE - 150}
                    fluid={isMobileViewport}
                    dimStyle={{background: 'rgba(0, 0, 0, 0.4)'}}
                    onVisibleChange={toggleDock}
                >
                    <div className="dock-content">
                        <div className="dock-header">
                            <h2>Language settings</h2>
                        </div>
                        <div className="dock-body">
                            <div className="language-settings">

                                <div className="language-select">
                                    <Select
                                        id="language-select"
                                        options={languageSelectOptions}
                                        value={selectedLanguage}
                                        onChange={this.changeLanguage}
                                    />
                                </div>
                                <div className="language-control">
                                    <Button
                                        type="submit"
                                        onClick={this.saveSettings}
                                        disabled={!notSavedChanges}
                                    >
                                        Save changes
                                    </Button>
                                    <Button
                                        type="submit"
                                        onClick={this.resetSettings}
                                        disabled={languageSettings && !languageSettings.timestamp}
                                    >
                                        Reset language
                                    </Button>
                                </div>
                                {
                                    languageSettings &&
                                    <h3>{languageSettings.longLabel}</h3>
                                }

                                {
                                    languageSettings && languageSettings.charMap && languageSettings.charMap.length > 0 &&
                                    <form className="char-map">
                                        <div className="char-single char-map-head">
                                            <div className="char-checkbox"/>
                                            <div className="char-inputs">
                                                <div className="char-from">From</div>
                                                <div className="char-convert-icon">→</div>
                                                <div className="char-to">To</div>
                                            </div>
                                            <div className="char-control"/>
                                        </div>
                                        {
                                            languageSettings.charMap.map((value, index) =>
                                                <div key={index} className="char-single">
                                                    <div className="char-checkbox">
                                                        <input
                                                            type="checkbox"
                                                            checked={!value.disabled}
                                                            onChange={() => this.toggleChar(index)}
                                                        />
                                                    </div>
                                                    <div className="char-inputs">
                                                        <div className="char-from">{value.from}</div>
                                                        <div className="char-convert-icon">→</div>
                                                        <div className="char-to">{value.to}</div>
                                                    </div>
                                                    <div className="char-control">
                                                        <span className="char-delete"
                                                            onClick={() => this.removeChar(index)}
                                                        >x</span>
                                                    </div>
                                                </div>
                                            )
                                        }

                                    </form>
                                }
                            </div>
                        </div>
                    </div>
                    <Button
                        className="dock-controller"
                        onClick={toggleDock}
                    >
                        Settings
                    </Button>
                </Dock>
                <Button
                    className="dock-controller dock-controller-static"
                    onClick={toggleDock}
                >
                    Settings
                </Button>
            </div>
        );
    }
}