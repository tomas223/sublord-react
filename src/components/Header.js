import React, {Component} from 'react';
import Button from "./Button";


class HeaderComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }


    render() {
        const {toggleDock, language} = this.props;
        return (
            <header className="header container">
                <div className="logo">
                    <a href="/">
                        <h1>SubLord</h1>
                    </a>
                </div>
                <div className="settings-info">
                    <Button
                        className="chosen-language-icon"
                        onClick={() => toggleDock()}
                        inverted
                    >
                        {
                            language && language.id
                        }
                    </Button>
                </div>
            </header>
        )
    }
}

export default HeaderComponent;