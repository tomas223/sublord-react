import React, {Component} from 'react';
import DownloadLink from "react-download-link";
import {convertFile} from "../utils/fileUtils";
import DownloadFileIconComponent from "../graphics/DownloadFileIconComponent";
import Button from "./Button";


class DownloadFileComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editedFiles: []
        }
    }

    convertFiles = async () => {
        const {files, languageSettings} = this.props;
        const {charMap, encoding} = languageSettings;
        const editedFiles = [];

        for (let file of files) {
            const newFile = await convertFile(file, charMap, encoding);
            editedFiles.push(newFile);
        }
        this.setState({editedFiles});
    };

    filesExist() {
        const {editedFiles} = this.state;
        return editedFiles && editedFiles.length > 0;
    }

    render() {
        const {editedFiles} = this.state;

        const editedFilesList = this.filesExist()
            ?
            <article className="download-file-list">
                <h3>Fixed files</h3>
                {
                    editedFiles.map(file => (
                        <div className="download-link" key={"fixed" + file.name}>
                            <DownloadLink
                                filename={file.name}
                                label={
                                    <div>
                                        {/*<span className="download-icon">*/}
                                            <DownloadFileIconComponent/>
                                        {/*</span>*/}
                                        {/*<img src={fileIcon} alt="File icon"/>*/}
                                        <span className="download-text">{file.name}</span>
                                    </div>
                                }
                                exportFile={() => file}
                                style={null}
                            />
                        </div>
                    ))
                }
            </article>
            :
            null;

        return (
            <section className="download-file">
                <article className="download-control">
                    <Button onClick={this.convertFiles}>
                        Remove diacritics
                    </Button>
                </article>
                {editedFilesList}
            </section>
        );
    }
}

export default DownloadFileComponent;