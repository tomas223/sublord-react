import React from "react";
import classNames from "classnames";

const Button = ({inverted, active, onClick, children, className, disabled, ...otherProps}) =>
    <button
        className={
            classNames(
                className,
                "btn",
                {"btn-inverted": inverted},
                {"btn-active": active},
                {"btn-disabled": disabled}
            )
        }
        onClick={onClick}
        disabled={disabled}
    >
        {
            children
        }
    </button>;

export default Button;