import React, {Component} from 'react';
import classNames from 'classnames';
import Dropzone from 'react-dropzone';
import DragAndDropIcon from "../graphics/DragAndDropIconComponent";
import ClickIconComponent from "../graphics/ClickIconComponent";

class DropFileComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rejectedFiles: []
        }
    }

    onDrop(acceptedFiles, rejectedFiles) {
        const {onNewFiles} = this.props;

        if (rejectedFiles && rejectedFiles.length > 0) {
            console.log('Some files were rejected', rejectedFiles);
            this.setState({rejectedFiles});
        }

        onNewFiles(acceptedFiles);

    }

    clearFiles() {
        this.onDrop([], null);
    }

    filesExist() {
        const {files} = this.props;
        return files && files.length > 0;
    }

    render() {
        const {files} = this.props;

        const dropzoneContent = this.filesExist()
            ?
            <div className="dropped-files">
                <h3>Dropped files:</h3>
                {
                    files.map(f =>
                        <p key={"drop" + f.name}>
                            {f.name} - {f.size} bytes
                        </p>
                    )
                }
            </div>
            :
            <div className="dropzone-info">
                <div className="graphics">
                    <div className="drag-drop-icon"><DragAndDropIcon/></div>
                    <div>or</div>
                    <div className="click-icon"><ClickIconComponent/></div>
                </div>
                <p>Drop some text (subtitles) files here, or click to select files to process.</p>
            </div>;


        return (
            <section className={classNames("drop-file", {"files-dropped": this.filesExist()})}>
                <div className="drop-file-header">
                    {
                        this.filesExist()
                        && <div
                            onClick={() => this.clearFiles()}
                            className="clear-files-button"
                        >clear files X</div>
                    }
                </div>
                <div className="dropzone-wrap">
                    <Dropzone
                        className="dropzone"
                        onDrop={(a, f) => this.onDrop(a, f)}
                    >
                        {dropzoneContent}
                    </Dropzone>
                </div>
            </section>
        );
    }
}

export default DropFileComponent;